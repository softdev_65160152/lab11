/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.database;

import com.mycompany.database.dao.UserDao;
import com.mycompany.database.helper.DatabaseHelper;
import com.mycompany.database.model.User;



/**
 *
 * @author USER
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for(User u: userDao.getAll()){
            System.out.println(u);
        }
        //User user1 = userDao.get(2);
        //System.out.println(user1);
        
        //User newUser = new User("user3", "password", 2, "F");
        //User insertedUser = userDao.save(newUser);
        //System.out.println(insertedUser);
        //insertedUser.setGender("M");
        //user1.setGender("M");
        //userDao.update(user1);
        //User updateUser = userDao.get(user1.getId());
        //System.out.println(updateUser);
        
        //delete
        //userDao.delete(user1);
        for(User u: userDao.getAll()){
            System.out.println(u);
        }

        for(User u: userDao.getAll("user_name like 'u%' ", " user_name asc, user_gender desc ")){
            System.out.println(u);
        }
        
        DatabaseHelper.close();
    }
}
